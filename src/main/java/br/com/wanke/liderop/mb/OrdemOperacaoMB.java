package br.com.wanke.liderop.mb;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Named;

import br.com.wanke.liderop.model.Apontamento;
import br.com.wanke.liderop.model.Colaborador;
import br.com.wanke.liderop.model.Ponto;

@Named
@RequestScoped
public class OrdemOperacaoMB {

	private int codigoSelecionado;

	public int getCodigoSelecionado() {
		return codigoSelecionado;
	}

	public void setCodigoSelecionado(int codigoSelecionado) {
		this.codigoSelecionado = codigoSelecionado;
	}

	@Named
	public List<Apontamento> apontamentos() {

		Apontamento a;
		List<Apontamento> retorno = new ArrayList<Apontamento>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm");

		Date data = null;

		try {
			data = sdf.parse("10/7/2014 08:00");
		} catch (ParseException e1) {
		}
		for (int i = 0; i < 500; i++) {

			a = new Apontamento();
			a.setCodProdutoAcabado(11234 + i);
			a.setDataHora(data);

			if (i > 400) {
				a.setSinc(false);
			} else {
				a.setSinc(true);
			}

			retorno.add(a);
			data = new Date(data.getTime() + (10 * 60000));
		}

		return retorno;
	}

	@Produces
	@Named
	public List<Apontamento> apontamentosInvalidos() {

		Apontamento a;
		List<Apontamento> retorno = new ArrayList<Apontamento>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm");

		Date data = null;

		try {
			data = sdf.parse("10/7/2014 08:00");
		} catch (ParseException e1) {
		}
		for (int i = 0; i < 100; i++) {

			a = new Apontamento();
			a.setCodProdutoAcabado(11234 + i);
			a.setDataHora(data);

			if (i > 400) {
				a.setSinc(false);
			} else {
				a.setSinc(true);
			}

			retorno.add(a);
			data = new Date(data.getTime() + (10 * 60000));
		}

		return retorno;
	}

	@Produces
	@Named
	public List<Colaborador> colaboradores() {

		Colaborador colaborador;
		List<Colaborador> retorno = new ArrayList<Colaborador>();

		for (int i = 0; i < 5; i++) {

			colaborador = new Colaborador();
			colaborador.setCod(11234 + i);
			colaborador.setNome("Colaborador " + i);
			retorno.add(colaborador);
		}

		return retorno;
	}

	@Produces
	@Named
	public List<Ponto> pontosColaborador() {

		Ponto ponto;
		List<Ponto> retorno = new ArrayList<Ponto>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm");

		Date data = null;
		try {
			data = sdf.parse("10/7/2014 08:00");
		} catch (ParseException e1) {
		}
		for (int i = 0; i < 4; i++) {

			ponto = new Ponto();
			ponto.setData(data);
			retorno.add(ponto);
			data = new Date(data.getTime() + (4 * 3600000));
		}

		return retorno;
	}

}
