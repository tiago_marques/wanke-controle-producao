package br.com.wanke.liderop.mb;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import br.com.wanke.liderop.model.ProgramacaoProducao;

@Named
@RequestScoped
public class ProducaoMB {

	public List<ProgramacaoProducao> programacoesProducao() {

		final ArrayList<ProgramacaoProducao> retorno = new ArrayList<ProgramacaoProducao>();

		final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm");

		for (int i = 0; i < 5; i++) {
			final ProgramacaoProducao programacao = new ProgramacaoProducao();
			programacao.setOrdem(852 + i);
			try {
				programacao.setDataHoraInicio(sdf.parse("10/7/2014 12:10"));
				programacao.setDataHoraFim(sdf.parse("15/7/2014 07:22"));
			} catch (final ParseException e) {
			}
			programacao.setCodigo(11210019l);
			programacao.setDescricao("CENTRIFUGA INOVA III LILAS 127V 60Hz");
			programacao.setQtdMaquinas(252);
			programacao.setQtdFuncionarios(9);
			retorno.add(programacao);
		}

		return retorno;
	}

}
