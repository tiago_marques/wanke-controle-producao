package br.com.wanke.liderop.mb;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import br.com.wanke.liderop.model.OrdemProducao;

@Named
@RequestScoped
public class DashboardBean {

	public List<OrdemProducao> ordensProducao() {

		ArrayList<OrdemProducao> retorno = new ArrayList<OrdemProducao>();

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		for (int i = 0; i < 5; i++) {
			OrdemProducao op = new OrdemProducao();
			op.setCodigo(123456l + i);
			try {
				op.setInicio(sdf.parse("10/7/2014"));
				op.setFim(sdf.parse("15/7/2014"));
			} catch (ParseException e) {
			}
			op.setQtdProdutoAcabado(500);
			op.setQtdProdutoAcabadoFinalizado(300);
			retorno.add(op);
		}

		return retorno;
	}
}
