package br.com.wanke.liderop.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "ordemProducao")
public class OrdemProducao {

	@Id
	@GeneratedValue
	private Long id;

	private Long codigo;
	private Date inicio;
	private Date fim;
	private int qtdProdutoAcabado;
	private int qtdProdutoAcabadoFinalizado;

	@Transient
	private List<Apontamento> apontamentos;

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public Date getInicio() {
		return inicio;
	}

	public void setInicio(Date inicio) {
		this.inicio = inicio;
	}

	public Date getFim() {
		return fim;
	}

	public void setFim(Date fim) {
		this.fim = fim;
	}

	public int getQtdProdutoAcabado() {
		return qtdProdutoAcabado;
	}

	public void setQtdProdutoAcabado(int qtdProdutoAcabado) {
		this.qtdProdutoAcabado = qtdProdutoAcabado;
	}

	public int getQtdProdutoAcabadoFinalizado() {
		return qtdProdutoAcabadoFinalizado;
	}

	public void setQtdProdutoAcabadoFinalizado(int qtdProdutoAcabadoFinalizado) {
		this.qtdProdutoAcabadoFinalizado = qtdProdutoAcabadoFinalizado;
	}

	public List<Apontamento> getApontamentos() {
		return apontamentos;
	}

	public void setApontamentos(List<Apontamento> apontamentos) {
		this.apontamentos = apontamentos;
	}

}
