package br.com.wanke.liderop.model;

import java.util.Date;

public class ProgramacaoProducao {

	private int ordem;
	private long codigo;
	private String descricao;
	private int qtdMaquinas;
	private Date dataHoraInicio;
	private Date dataHoraFim;
	private int qtdFuncionarios;

	public int getOrdem() {
		return this.ordem;
	}

	public void setOrdem(final int ordem) {
		this.ordem = ordem;
	}

	public long getCodigo() {
		return this.codigo;
	}

	public void setCodigo(final long codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(final String descricao) {
		this.descricao = descricao;
	}

	public int getQtdMaquinas() {
		return this.qtdMaquinas;
	}

	public void setQtdMaquinas(final int qtdMaquinas) {
		this.qtdMaquinas = qtdMaquinas;
	}

	public Date getDataHoraInicio() {
		return this.dataHoraInicio;
	}

	public void setDataHoraInicio(final Date dataHoraInicio) {
		this.dataHoraInicio = dataHoraInicio;
	}

	public Date getDataHoraFim() {
		return this.dataHoraFim;
	}

	public void setDataHoraFim(final Date dataHoraFim) {
		this.dataHoraFim = dataHoraFim;
	}

	public int getQtdFuncionarios() {
		return this.qtdFuncionarios;
	}

	public void setQtdFuncionarios(final int qtdFuncionarios) {
		this.qtdFuncionarios = qtdFuncionarios;
	}

}
