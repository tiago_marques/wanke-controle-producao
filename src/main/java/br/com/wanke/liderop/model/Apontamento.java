package br.com.wanke.liderop.model;

import java.util.Date;

public class Apontamento {

	private long codProdutoAcabado;
	private Date dataHora;
	private boolean sinc;
	private boolean invalido;

	public long getCodProdutoAcabado() {
		return codProdutoAcabado;
	}

	public void setCodProdutoAcabado(long codProdutoAcabado) {
		this.codProdutoAcabado = codProdutoAcabado;
	}

	public Date getDataHora() {
		return dataHora;
	}

	public void setDataHora(Date dataHora) {
		this.dataHora = dataHora;
	}

	public boolean isSinc() {
		return sinc;
	}

	public void setSinc(boolean sinc) {
		this.sinc = sinc;
	}

	public boolean isInvalido() {
		return invalido;
	}

	public void setInvalido(boolean invalido) {
		this.invalido = invalido;
	}

}
